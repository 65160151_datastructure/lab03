/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab03;

/**
 *
 * @author User
 */
public class Lab03 {

         public static void main(String[] args) {
        
        int[] nums1 = {1,2,3,0,0,0};
        int[] nums2 = {2,5,0,6,0};
        
        int m = 3,n = 3;
        
        int length1 = nums1.length;
        int length2 = nums2.length;
        int j = 0;

        int numChangePositon;
        
        //the last n elements are set to 0 and should be ignored
        for(int i=0;i<length1;i++){
            
            if(nums1[i] == 0){
                while(nums2[j] == 0){
                    j++;
                }
                nums1[i] = nums2[j];
                j++;
                if(nums2[j-1]<=nums1[i-1] && i > 0){
                    
                    numChangePositon = nums1[i-1];
                    nums1[i-1]=nums2[j-1];
                    nums1[i]=numChangePositon;
                }
                
            }
            
            System.out.print(nums1[i]+" ");
            
        }

        System.out.println();
    }
}
