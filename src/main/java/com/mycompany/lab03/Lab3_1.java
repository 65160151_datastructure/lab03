/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab03;

/**
 *
 * @author User
 */
public class Lab3_1 {
     public static int Sqrt(double x){
        double left = 0;
        double right = x;
        double epsilon = 0.00001;
        double mid =0;

        while (left<=right) {
            mid =left + (right - left)/2;
            if(mid*mid == x || ((mid*mid) > x && (mid*mid) - x <= epsilon)){
                return (int) mid;
            }else if(mid*mid < x){
                left = mid +epsilon;
            }else{
                right = mid -epsilon;
            }
        }
        return (int) (mid+epsilon);
    }

    public static void main(String[] args) {
        double x = 30;
        int sqrtX = Sqrt(x);
        
        System.out.println("Output = "+sqrtX);
        
    }
}
